<?php

return [
    'Login' => 'Логин',
    'First Name' => 'Имя',
    'Second Name' => 'Фамилия',
    'Submit' => 'Отправить',
    'Register' => 'Регистрация',
    'You have successfully registered!' => 'Вы успешно прошли регистрацию!',
    'You must agree to the registration agreement.' => 'Вы должны согласиться с регистрационным соглашением.',
    'I agree to the use of personal data.' => 'Даю согласие на использование персональных данных.',
    'Password' => 'Пароль',
    'Repeat password' => 'Повторите пароль',
    'Please enter a password' => 'Пожалуйста, введите пароль',
    'Your password must be at least characters' => 'Ваш пароль должен состоять хотя бы из символов',

    'Work' => 'Работа',
    'Work start date' => 'Дата начала работы',
    'Work end date' => 'Дата конца работы',
    'Work experience' => 'Опыт работы',
];