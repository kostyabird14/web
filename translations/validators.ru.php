<?php

return [
    'Your password must be at least {{ limit }} characters' => 'Ваш пароль должен быть не менее {{ limit }} символов',
    'Value of {{ label }} must be not empty' => 'Значение поля {{ label }} не должно быть пустым'
];