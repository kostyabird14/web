<?php

namespace App\Requests\University;

use App\Exceptions\ValidationException;
use App\Helper\ConstraintsHelper;
use App\Requests\BaseRequest;
use App\Traits\JsonRequestTrait;

class UpdatePartialRequest extends BaseRequest
{
    use JsonRequestTrait;

    /**
     * @return array[]
     */
    protected function rules(): array
    {
        return [
            'title' => [ConstraintsHelper::ruleType('string')],
            'students' => [ConstraintsHelper::ruleType('integer')],
            'license' => [ConstraintsHelper::ruleType('boolean')],
        ];
    }

    /**
     * @throws ValidationException
     */
    public function getTitle(): ?string
    {
        return $this->getValid('title');
    }

    /**
     * @throws ValidationException
     */
    public function getStudents(): ?int
    {
        return $this->getValid('students');
    }

    /**
     * @throws ValidationException
     */
    public function getLicense(): ?bool
    {
        return $this->getValid('license');
    }
}