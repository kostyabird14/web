<?php

namespace App\Requests\University;

use App\Exceptions\ValidationException;
use App\Helper\ConstraintsHelper;
use App\Requests\BaseRequest;
use App\Traits\JsonRequestTrait;

class StoreRequest extends BaseRequest
{
    use JsonRequestTrait;

    /**
     * @return array[]
     */
    protected function rules(): array
    {
        return [
            'title' => [
                ConstraintsHelper::ruleRequired(),
                ConstraintsHelper::ruleType('string'),
            ]
        ];
    }

    /**
     * @return string
     * @throws ValidationException
     */
    public function getTitle(): string
    {
        return $this->getValid('title');
    }
}