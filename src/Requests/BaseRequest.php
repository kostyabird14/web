<?php

namespace App\Requests;

use App\Exceptions\ValidationException;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints;

abstract class BaseRequest extends Request
{
    protected ?ValidatorInterface $validator = null;
    protected ?array $validated = null;

    /**
     * @param ValidatorInterface $validator
     * @return void
     */
    public function setValidator(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @throws ValidationException
     * @throws Exception
     */
    public function validate(): array
    {
        if (!$this->validator) {
            throw new Exception('Validator is not set');
        }

        if ($this->validated !== null) {
            $data = $this->validationData();

            $errors = $this->validator->validate($data, new Collection($this->rules()));

            if ($errors->count()) {

                $messages = [];

                foreach ($errors as $violation) {
                    /** @var ConstraintViolation $violation */
                    $messages[$violation->getPropertyPath()][] = $violation->getMessage();
                }

                throw ValidationException::withMessages($messages);
            }

            $this->validated = $data;
        }

        return $this->validated;
    }

    /**
     * @param string $param
     * @return mixed
     * @throws ValidationException
     * @throws Exception
     */
    protected function getValid(string $param): mixed
    {
        return $this->validate()[$param] ?? null;
    }

    /**
     * @return array
     */
    abstract protected function validationData(): array;

    /**
     * @return array
     */
    abstract protected function rules(): array;
}