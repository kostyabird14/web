<?php

namespace App\Form;

use App\Entity\JobExperience;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ExperienceFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Work',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Value of {{ label }} must be not empty',
                    ])
                ]
            ])
            ->add('experienceStart', DateType::class, [
                'label' => 'Work start date',
            ])
            ->add('experienceEnd', DateType::class, [
                'label' => 'Work end date'
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => JobExperience::class,
        ]);
    }
}