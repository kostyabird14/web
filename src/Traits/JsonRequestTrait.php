<?php

namespace App\Traits;

trait JsonRequestTrait
{
    /**
     * @return array
     */
    protected function validationData(): array
    {
        return $this->toArray();
    }
}