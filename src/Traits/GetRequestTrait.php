<?php

namespace App\Traits;

trait GetRequestTrait
{
    /**
     * @return array
     */
    protected function validationData(): array
    {
        return $this->query->all();
    }
}