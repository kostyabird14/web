<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\EducationRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;

class EducationController extends AbstractController
{
    const NORMALIZER_CONTEXT = [
        AbstractNormalizer::ATTRIBUTES => [
            'id',
            'status',
            'title',
            'createdAt',
            'publishedAt',
            'updatedAt'
        ],
        DateTimeNormalizer::FORMAT_KEY => 'Y-m-d H:i:s'
    ];

    #[Route('/api/education', name: 'app_education', methods: 'GET')]
    public function index(EducationRepository $repository): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        $educations = $repository->findAllForUser($user);

        return $this->json(
            data: $educations,
            context: self::NORMALIZER_CONTEXT
        );
    }

    /**
     * @param string $id
     * @param EducationRepository $repository
     * @return JsonResponse
     * @throws NonUniqueResultException
     */
    #[Route('/api/education/{id}', name: 'app_education_show', methods: 'GET')]
    public function show(string $id, EducationRepository $repository): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        $education = $repository->findOneForUser($user, $id);

        return $this->json(
            data: $education,
            context: self::NORMALIZER_CONTEXT
        );
    }

    #[Route('/api/education/{id}', name: 'app_education_store', methods: 'POST')]
    public function store(string $id): JsonResponse
    {
        return $this->json([]);
    }

    #[Route('/api/education/{id}', name: 'app_education_patch', methods: 'PATCH')]
    public function updatePartial(string $id): JsonResponse
    {
        return $this->json([]);
    }

    #[Route('/api/education/{id}', name: 'app_education_put', methods: 'PUT')]
    public function updateFull(string $id): JsonResponse
    {
        return $this->json([]);
    }

    #[Route('/api/education/{id}', name: 'app_education_delete', methods: 'DELETE')]
    public function delete(string $id): JsonResponse
    {
        return $this->json([]);
    }
}