<?php

namespace App\Controller;

use App\Entity\University;
use App\Exceptions\ValidationException;
use App\Factory\UniversityFactory;
use App\Repository\UniversityRepository;
use App\Requests\University\StoreRequest;
use App\Requests\University\UpdateFullRequest;
use App\Requests\University\UpdatePartialRequest;
use Doctrine\ORM\EntityManagerInterface;
use ReflectionException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UniversityController extends AbstractController
{
    /**
     * @param UniversityRepository $repository
     * @return JsonResponse
     */
    #[Route('/api/university', name: 'app_university', methods: 'GET')]
    public function index(UniversityRepository $repository): JsonResponse
    {
        return $this->json($repository->findAll());
    }

    /**
     * @param string $id
     * @param UniversityRepository $repository
     * @return JsonResponse
     * @throws NotFoundHttpException|ReflectionException
     */
    #[Route('/api/university/{id}', name: 'app_university_show', methods: 'GET')]
    public function show(string $id, UniversityRepository $repository): JsonResponse
    {
        return $this->json($repository->findOrThrow($id));
    }

    /**
     * @param ValidatorInterface $validator
     * @param EntityManagerInterface $manager
     * @return JsonResponse
     * @throws ValidationException
     */
    #[Route('/api/university', name: 'app_university_store', methods: 'POST')]
    public function store(ValidatorInterface $validator, EntityManagerInterface $manager): JsonResponse
    {
        $request = StoreRequest::createFromGlobals();
        $request->setValidator($validator);

        $university = UniversityFactory::build($request->getTitle());

        $manager->persist($university);
        $manager->flush();

        return $this->json($university);
    }

    /**
     * @param string $id
     * @param ValidatorInterface $validator
     * @param UniversityRepository $repository
     * @param EntityManagerInterface $manager
     * @return JsonResponse
     * @throws ReflectionException
     * @throws ValidationException
     */
    #[Route('/api/university/{id}', name: 'app_university_patch', methods: 'PATCH')]
    public function updatePartial(
        string                 $id,
        ValidatorInterface     $validator,
        UniversityRepository   $repository,
        EntityManagerInterface $manager
    ): JsonResponse
    {
        $request = UpdatePartialRequest::createFromGlobals();
        $request->setValidator($validator);

        /** @var University $university */
        $university = $repository->findOrThrow($id);

        if ($request->getTitle()) $university->setTitle($request->getTitle());
        if ($request->getStudents()) $university->setStudents($request->getStudents());
        if ($request->getLicense()) $university->setLicense($request->getLicense());

        $manager->persist($university);
        $manager->flush();

        return $this->json($university);
    }

    /**
     * @param string $id
     * @param ValidatorInterface $validator
     * @param UniversityRepository $repository
     * @param EntityManagerInterface $manager
     * @return JsonResponse
     * @throws ReflectionException
     * @throws ValidationException
     */
    #[Route('/api/university/{id}', name: 'app_university_put', methods: 'PUT')]
    public function updateFull(
        string                 $id,
        ValidatorInterface     $validator,
        UniversityRepository   $repository,
        EntityManagerInterface $manager
    ): JsonResponse
    {
        $request = UpdateFullRequest::createFromGlobals();
        $request->setValidator($validator);

        /** @var University $university */
        $university = $repository->findOrThrow($id);

        $university->setTitle($request->getTitle());
        $university->setStudents($request->getStudents());
        $university->setLicense($request->getLicense());

        $manager->persist($university);
        $manager->flush();

        return $this->json($university);
    }

    /**
     * @param string $id
     * @param EntityManagerInterface $entityManager
     * @param UniversityRepository $repository
     * @return JsonResponse
     * @throws ReflectionException
     */
    #[Route('/api/university/{id}', name: 'app_university_delete', methods: 'DELETE')]
    public function delete(string $id, EntityManagerInterface $entityManager, UniversityRepository $repository): JsonResponse
    {
        $university = $repository->findOrThrow($id);

        $entityManager->remove($university);
        $entityManager->flush();

        return $this->json(['message' => 'University deleted successfully']);
    }
}