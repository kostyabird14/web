<?php

namespace App\Controller;

use App\Entity\TextBlock;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AboutController extends AbstractController
{
    #[Route('/about', name: 'app_about', methods: ['GET'])]
    public function index(ManagerRegistry $doctrine): Response
    {
        $textBlocks = $doctrine
            ->getRepository(TextBlock::class)
            ->findAll();

        return $this->render('about/index.html.twig', [
            'text_blocks' => $textBlocks,
        ]);
    }

    #[Route('/about/store', name: 'app_about_store', methods: ['POST'])]
    public function store(ManagerRegistry $doctrine): JsonResponse
    {
        $textBlock = new TextBlock();
        $textBlock
            ->setTitle('test title 3')
            ->setContent('test content 3')
            ->setKeywords('test keywords 3');

        $doctrine
            ->getRepository(TextBlock::class)
            ->save($textBlock, true);

        return $this->json(['success' => true, 'data' => ['id' => $textBlock->getId()]]);
    }
}
