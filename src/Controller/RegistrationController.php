<?php

namespace App\Controller;

use App\Entity\User;
use App\Events\UserAfterAddEvent;
use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class RegistrationController extends AbstractController
{
    /**
     * @param Request $request
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    #[Route(path: '/register', name: 'app_register')]
    public function register(
        Request                     $request,
        UserPasswordHasherInterface $userPasswordHasher,
        EntityManagerInterface      $entityManager,
        EventDispatcherInterface    $eventDispatcher
    ): Response
    {
        $form = $this->createForm(RegistrationFormType::class, $user = new User());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $user->setConfirmationCode(md5(uniqid(rand(), true)));

            $entityManager->persist($user);
            $entityManager->flush();

            $userAfterAddEvent = new UserAfterAddEvent($user);
            $eventDispatcher->dispatch($userAfterAddEvent, UserAfterAddEvent::NAME);

            return $this->redirectToRoute('app_index');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    #[Route(path: '/register/confirm/{code}', name: 'app_register_confirm')]
    public function emailConfirmation(
        UserRepository         $userRepository,
        EntityManagerInterface $entityManager,
        string                 $code
    ): Response
    {
        /** @var User $user */
        $user = $userRepository->findOneBy(['confirmationCode' => $code]);

        if ($user === null) {
            return new Response('404');
        }

        $user->setEnabled(true);
        $user->setConfirmationCode('');

        $entityManager->flush();

        return $this->render('registration/account_confirm.html.twig', [
            'user' => $user,
        ]);
    }
}
