<?php

namespace App\Controller;

use App\Entity\JobExperience;
use App\Entity\User;
use App\Form\ExperienceFormType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExperienceController extends AbstractController
{
    /**
     * @throws Exception
     */
    #[Route('/experience', name: 'app_experience')]
    public function index(ManagerRegistry $doctrine): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        /** @var JobExperience[] $experiences */
        $experiences = $doctrine
            ->getRepository(JobExperience::class)
            ->user($user);

        return $this->render('experience/index.html.twig', [
            'experiences' => $experiences,
            'user' => $user
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route('/experience/create', name: 'app_experience_create')]
    public function create(Request $request, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $form = $this->createForm(type: ExperienceFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $experience = new JobExperience();

            $experience->setTitle($form->get('title')->getData());
            $experience->setExperienceStart($form->get('experienceStart')->getData());
            $experience->setExperienceEnd($form->get('experienceEnd')->getData());
            $experience->setUser($user);

            $entityManager->persist($experience);
            $entityManager->flush();

            return $this->redirectToRoute('app_experience');
        }

        return $this->render('experience/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}