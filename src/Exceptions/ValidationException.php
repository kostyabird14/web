<?php

namespace App\Exceptions;

use Exception;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class ValidationException extends Exception
{
    private array $errors;

    #[Pure]
    public function __construct(
        string $message = 'Validation error',
        int $code = Response::HTTP_UNPROCESSABLE_ENTITY,
        ?Throwable $previous = null,
        array $errors = []
    )
    {
        parent::__construct($message, $code, $previous);

        $this->errors = $errors;
    }

    /**
     * Create a new validation exception from a plain array of messages.
     *
     * @param array $messages
     * @return static
     */
    #[Pure]
    public static function withMessages(array $messages = []): static
    {
        return new static(errors: $messages);
    }

    /**
     * Get all the validation error messages.
     *
     * @return array
     */
    public function errors(): array
    {
        return $this->errors;
    }
}