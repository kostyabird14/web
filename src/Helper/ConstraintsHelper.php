<?php

namespace App\Helper;

use Symfony\Component\Validator\Constraints;

class ConstraintsHelper
{
    /**
     * @return Constraints\Required
     */
    public static function ruleRequired(): Constraints\Required
    {
        return new Constraints\Required();
    }

    /**
     * @param string $type
     * @return Constraints\Type
     */
    public static function ruleType(string $type): Constraints\Type
    {
        return new Constraints\Type([
            'type' => $type,
            'message' => 'The value {{ value }} is not a valid {{ type }}.',
        ]);
    }
}