<?php

namespace App\Commands;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateRealUserCommand extends Command
{
    public function __construct(
        private UserPasswordHasherInterface $userPasswordHasher,
        private EntityManagerInterface $entityManager
    )
    {
        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('user:real-create')
            ->setDescription('Create user.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $user = new User();

        $n = rand();

        //security.password_hasher
        $user->setLogin('User #' . $n);
        $user->setFirstName('First Name #' . $n);
        $user->setLastname('Last Name #' . $n);
        $user->setEmail('Email #' . $n);
        $user->setEnabled(true);
        $user->setPassword(
            $this->userPasswordHasher->hashPassword(
                $user,
                'Password #' . $n
            )
        );

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}