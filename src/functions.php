<?php

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

if (!function_exists('date_now')) {
    /**
     * @throws Exception
     */
    function date_now(): DateTimeImmutable
    {
        return new DateTimeImmutable('now', new DateTimeZone('Europe/Moscow'));
    }

    /**
     * @param string $message
     * @param mixed|null $data
     * @param int $status
     * @return JsonResponse
     */
    function json_answer(string $message = '', mixed $data = null, int $status = Response::HTTP_OK): JsonResponse {
        return new JsonResponse(['data' => $data, 'message' => $message], $status);
    }
}