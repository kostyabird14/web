<?php

namespace App\Factory;

use App\Entity\University;
use Ramsey\Uuid\Uuid;

class UniversityFactory
{
    public static function build(string $title): University
    {
        $university = new University();

        $university->setId(Uuid::uuid7()->toString());
        $university->setTitle($title);

        return $university;
    }
}