<?php

namespace App\Factory;

use App\Entity\Education;
use App\Entity\User;
use Doctrine\ORM\PersistentCollection;
use Exception;

class EducationFactory
{
    /**
     * @param string $title
     * @param User $user
     * @param PersistentCollection $universities
     * @return Education
     * @throws Exception
     */
    public static function build(string $title, User $user, PersistentCollection $universities): Education
    {
        $date = date_now();

        $ed = new Education();
        $ed->setTitle($title);
        $ed->setCreatedAt($date);
        $ed->setUpdatedAt($date);
        $ed->setStatus(Education::DRAFT);
        $ed->setUser($user);
        $ed->setUniversities($universities);

        return $ed;
    }

    /**
     * @param string|null $title
     * @param User|null $user
     * @param PersistentCollection|null $universities
     * @return Education
     * @throws Exception
     */
    public static function buildDraft(?string $title= null, ?User $user = null, ?PersistentCollection $universities = null): Education
    {
        $date = date_now();

        $ed = new Education();
        if ($title) $ed->setTitle($title);
        $ed->setCreatedAt($date);
        $ed->setUpdatedAt($date);
        $ed->setStatus(Education::DRAFT);
        if ($user) $ed->setUser($user);
        if ($universities) $ed->setUniversities($universities);

        return $ed;
    }
}