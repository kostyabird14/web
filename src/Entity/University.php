<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

#[ORM\Entity]
#[ORM\Table(name: 'universities')]
class University
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\CustomIdGenerator(class: 'Ramsey\Uuid\Doctrine\UuidV7Generator')]
    private ?string $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column]
    private ?int $students = null;

    #[ORM\Column]
    private bool $license = false;

    #[ORM\ManyToMany(targetEntity: Education::class, mappedBy: 'universities')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private PersistentCollection $educations;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     */
    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return int|null
     */
    public function getStudents(): ?int
    {
        return $this->students;
    }

    /**
     * @param int|null $students
     */
    public function setStudents(?int $students): void
    {
        $this->students = $students;
    }

    /**
     * @return bool
     */
    public function isLicense(): bool
    {
        return $this->license;
    }

    /**
     * @param bool $license
     */
    public function setLicense(bool $license): void
    {
        $this->license = $license;
    }

    /**
     * @return PersistentCollection
     */
    public function getEducations(): PersistentCollection
    {
        return $this->educations;
    }

    /**
     * @param PersistentCollection $educations
     */
    public function setEducations(PersistentCollection $educations): void
    {
        $this->educations = $educations;
    }
}