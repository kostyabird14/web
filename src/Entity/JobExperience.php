<?php

namespace App\Entity;

use App\Repository\JobExperienceRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: JobExperienceRepository::class)]
class JobExperience
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(type: 'date_immutable')]
    private ?DateTimeImmutable $experienceStart = null;

    #[ORM\Column(type: 'date_immutable')]
    private ?DateTimeImmutable $experienceEnd = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'experiences')]
    private ?User $user = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return DateTime|null
     */
    public function getExperienceStart(): ?DateTimeImmutable
    {
        return $this->experienceStart;
    }

    /**
     * @param DateTime|null $experienceStart
     */
    public function setExperienceStart(?DateTime $experienceStart): void
    {
        $this->experienceStart = DateTimeImmutable::createFromMutable($experienceStart);
    }

    /**
     * @return DateTime|null
     */
    public function getExperienceEnd(): ?DateTimeImmutable
    {
        return $this->experienceEnd;
    }

    /**
     * @param DateTime|null $experienceEnd
     */
    public function setExperienceEnd(?DateTime $experienceEnd): void
    {
        $this->experienceEnd = DateTimeImmutable::createFromMutable($experienceEnd);
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     */
    public function setUser(?User $user): void
    {
        $this->user = $user;
    }
}