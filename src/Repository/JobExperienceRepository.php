<?php

namespace App\Repository;

use App\Entity\JobExperience;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<JobExperience>
 *
 * @method JobExperience|null find($id, $lockMode = null, $lockVersion = null)
 * @method JobExperience|null findOneBy(array $criteria, array $orderBy = null)
 * @method JobExperience[]    findAll()
 * @method JobExperience[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobExperienceRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JobExperience::class);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function user(User $user): mixed
    {
        return $this->createQueryBuilder('e')
            ->where('e.user = :user')
            ->setParameter('user', $user->getId())
            ->getQuery()
            ->execute();
    }
}