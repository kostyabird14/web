<?php

namespace App\Repository;

use App\Entity\Education;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;

class EducationRepository
{
    /**
     * @var EntityRepository
     */
    private EntityRepository $repository;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->repository = $em->getRepository(Education::class);
    }

    /**
     * @param User $user
     * @return Education[]
     */
    public function findAllForUser(User $user): array
    {
        return $this
            ->repository
            ->createQueryBuilder('e')
            ->where('e.user = :user')
            ->setParameter('user', $user->getId())
            ->getQuery()
            ->getResult();
    }

    /**
     * @param User $user
     * @param string $id
     * @return Education
     * @throws NonUniqueResultException
     */
    public function findOneForUser(User $user, string $id): Education
    {
        return $this
            ->repository
            ->createQueryBuilder('e')
            ->where('e.id = :id AND e.user = :user')
            ->setParameter('id', $id)
            ->setParameter('user', $user->getId())
            ->getQuery()
            ->getOneOrNullResult();
    }
}
