<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

abstract class BaseRepository extends ServiceEntityRepository
{
    /**
     * @param mixed $id
     * @param int|null $lockMode
     * @param int|null $lockVersion
     * @return object|null
     * @throws ReflectionException
     * @throws NotFoundHttpException
     */
    public function findOrThrow(mixed $id, ?int $lockMode = null, ?int $lockVersion = null): object|null
    {
        $entity = $this->find($id, $lockMode, $lockVersion);

        if (!$entity) {
            $class = new ReflectionClass($this->_entityName);

            throw new NotFoundHttpException($class->getShortName() . ' not found');
        }

        return $entity;
    }
}