<?php

namespace App\Subscribers;

use App\Exceptions\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use function json_answer;

class ExceptionSubscriber
{
    public function __invoke(ExceptionEvent $event): void
    {
        $e = $event->getThrowable();
        $request = $event->getRequest();

        if ($request->headers->get('Accept') === 'application/json') {

            $response = match (true) {
                $e instanceof NotFoundHttpException => json_answer(
                    message: $e->getMessage(),
                    status: Response::HTTP_NOT_FOUND
                ),
                $e instanceof ValidationException => json_answer(
                    message: $e->getMessage(),
                    data: $e->errors(),
                    status: Response::HTTP_UNPROCESSABLE_ENTITY
                ),
                default => null
            };

            if (!$response instanceof JsonResponse) {
                $response = json_answer(message: 'Server error', status: $e->getCode());
            }

            // HttpExceptionInterface is a special type of exception that
            // holds status code and header details
            if ($e instanceof HttpExceptionInterface && !$e instanceof NotFoundHttpException) {
                $response->headers->replace($e->getHeaders());
            }

            $event->setResponse($response);
        }
    }
}