<?php

namespace App\Subscribers;

use App\Events\UserAfterAddEvent;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\NoReturn;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mime\Email;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class UserMailerSubscriber implements EventSubscriberInterface
{
    const FROM_ADDRESS = 'test@example.com';

    /**
     * @param Environment $twig
     * @param TranslatorInterface $translator
     */
    public function __construct(
//        private MailerInterface     $mailer,
        private Environment         $twig,
        private TranslatorInterface $translator
    )
    {
    }

    /**
     * @return string[]
     */
    #[ArrayShape([UserAfterAddEvent::NAME => "string"])]
    public static function getSubscribedEvents(): array
    {
        return [
            UserAfterAddEvent::NAME => 'onUserAdd'
        ];
    }

    /**
     * @param UserAfterAddEvent $event
     * @return void
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    #[NoReturn]
    public function onUserAdd(UserAfterAddEvent $event)
    {
        $user = $event->getUser();

        $email = (new Email())
            ->from(self::FROM_ADDRESS)
            ->to($user->getEmail())
            ->subject($this->translator->trans('You have successfully registered!'))
            ->html($this->twig->render('registration/confirmation.html.twig', [
                'user' => $user
            ]));

        // $this->mailer->send($email);
    }
}