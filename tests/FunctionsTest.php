<?php

namespace App\Tests;

use DateTimeZone;
use Exception;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

class FunctionsTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function test_date_now()
    {
        $this->assertTrue(
            date("Y-m-d H")
            ===
            date_now()
                ->setTimezone(new DateTimeZone(date_default_timezone_get()))
                ->format('Y-m-d H')
        );
    }

    /**
     * @throws Exception
     */
    public function test_json_answer()
    {
        $answer = json_answer(message: 'some message', data: ['some data'], status: Response::HTTP_FORBIDDEN);

        $this->assertTrue($answer->getStatusCode() === Response::HTTP_FORBIDDEN);
        $this->assertTrue($answer->getContent() === '{"data":["some data"],"message":"some message"}');
    }
}