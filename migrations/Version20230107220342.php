<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230107220342 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE educations (id UUID NOT NULL, user_id INT NOT NULL, status VARCHAR(255) NOT NULL, title VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, published_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_730876ADA76ED395 ON educations (user_id)');
        $this->addSql('COMMENT ON COLUMN educations.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN educations.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN educations.published_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN educations.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE education_university (education_id UUID NOT NULL, university_id UUID NOT NULL, PRIMARY KEY(education_id, university_id))');
        $this->addSql('CREATE INDEX IDX_FD8672C12CA1BD71 ON education_university (education_id)');
        $this->addSql('CREATE INDEX IDX_FD8672C1309D1878 ON education_university (university_id)');
        $this->addSql('COMMENT ON COLUMN education_university.education_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN education_university.university_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE universities (id UUID NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN universities.id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE educations ADD CONSTRAINT FK_730876ADA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE education_university ADD CONSTRAINT FK_FD8672C12CA1BD71 FOREIGN KEY (education_id) REFERENCES educations (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE education_university ADD CONSTRAINT FK_FD8672C1309D1878 FOREIGN KEY (university_id) REFERENCES universities (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE educations DROP CONSTRAINT FK_730876ADA76ED395');
        $this->addSql('ALTER TABLE education_university DROP CONSTRAINT FK_FD8672C12CA1BD71');
        $this->addSql('ALTER TABLE education_university DROP CONSTRAINT FK_FD8672C1309D1878');
        $this->addSql('DROP TABLE educations');
        $this->addSql('DROP TABLE education_university');
        $this->addSql('DROP TABLE universities');
    }
}
