<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230102121925 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE job_experience_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE job_experience (id INT NOT NULL, user_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, experience_start DATE NOT NULL, experience_end DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6FE7511EA76ED395 ON job_experience (user_id)');
        $this->addSql('COMMENT ON COLUMN job_experience.experience_start IS \'(DC2Type:date_immutable)\'');
        $this->addSql('COMMENT ON COLUMN job_experience.experience_end IS \'(DC2Type:date_immutable)\'');
        $this->addSql('ALTER TABLE job_experience ADD CONSTRAINT FK_6FE7511EA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cron_job ALTER id DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE job_experience_id_seq CASCADE');
        $this->addSql('ALTER TABLE job_experience DROP CONSTRAINT FK_6FE7511EA76ED395');
        $this->addSql('DROP TABLE job_experience');
        $this->addSql('CREATE SEQUENCE cron_job_id_seq');
        $this->addSql('SELECT setval(\'cron_job_id_seq\', (SELECT MAX(id) FROM cron_job))');
        $this->addSql('ALTER TABLE cron_job ALTER id SET DEFAULT nextval(\'cron_job_id_seq\')');
    }
}
